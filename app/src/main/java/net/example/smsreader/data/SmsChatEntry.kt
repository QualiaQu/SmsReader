package net.example.smsreader.data

data class SmsChatEntry(
    val address: String,
    val messages: List<String>
)

data class Message(
    val address: String,
    val message: String
)
