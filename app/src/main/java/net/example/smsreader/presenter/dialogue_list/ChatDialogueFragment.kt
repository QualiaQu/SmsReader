package net.example.smsreader.presenter.dialogue_list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import net.example.smsreader.R
import net.example.smsreader.databinding.FragmentChatDialogueBinding

class ChatDialogueFragment : Fragment(R.layout.fragment_chat_dialogue) {

    private val args: ChatDialogueFragmentArgs by navArgs()
    private val binding: FragmentChatDialogueBinding by viewBinding()
    private val viewModel: ChatDialogueViewModel by viewModels()
    private val adapter = ChatDialogueAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.dialogueMessages.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        initializeUI()
        initializeRecycler()
        viewModel.getDialogueMessages(args.address, args.messages.toList())
    }

    private fun initializeUI() {
        initializeRecycler()
    }

    private fun initializeRecycler() = with(binding.smsListRecycler) {
        layoutManager = LinearLayoutManager(requireContext())
        adapter = this@ChatDialogueFragment.adapter
        addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                LinearLayoutManager.VERTICAL
            )
        )
    }
}