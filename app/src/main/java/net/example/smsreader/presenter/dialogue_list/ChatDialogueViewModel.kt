package net.example.smsreader.presenter.dialogue_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.example.smsreader.data.Message

class ChatDialogueViewModel: ViewModel() {

    private val _dialogueMessages = MutableLiveData<List<Message>>()
    val dialogueMessages: LiveData<List<Message>>
        get() = _dialogueMessages


    fun getDialogueMessages(address: String, messages: List<String>) {
        val dialogueList = messages.map { message ->
            Message(address, message)
        }
        _dialogueMessages.postValue(dialogueList)
    }
}