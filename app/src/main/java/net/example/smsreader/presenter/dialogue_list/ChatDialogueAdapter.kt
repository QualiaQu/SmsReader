package net.example.smsreader.presenter.dialogue_list

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import net.example.smsreader.R
import net.example.smsreader.data.Message
import net.example.smsreader.databinding.DialogueItemBinding

class ChatDialogueAdapter() : RecyclerView.Adapter<ChatDialogueAdapter.ChatViewHolder>() {

    private val list = mutableListOf<Message>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding = DialogueItemBinding.inflate(layoutInflater, parent, false)
        return ChatViewHolder(binding)
    }

    override fun getItemCount(): Int =
        list.size

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun submitList(list: List<Message>) = with(this.list) {
        clear()
        addAll(list)
        notifyDataSetChanged()
    }

    inner class ChatViewHolder(
        private val binding: DialogueItemBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(entry: Message) = with(binding) {
            val context = binding.root.context

            smsAvatar.background = getRandomBackground(context)
            smsAvatar.text = entry.address.first().toString()

            smsSender.text = entry.address
            messageAgenda.text = entry.message
        }

        private fun getRandomBackground(context: Context): Drawable? {
            val background =
                ContextCompat.getDrawable(context, R.drawable.sms_chat_entry_avatar)
            val color = context.resources.getIntArray(R.array.rainbow).random()
            background?.let { DrawableCompat.setTint(it, color) }
            return background
        }
    }
}